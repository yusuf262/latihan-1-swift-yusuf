//
//  ViewController.swift
//  Latihan1
//
//  Created by MacBook Pro on 17/03/23.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var astronoutButton: UIButton!
    @IBOutlet weak var rockStarButton: UIButton!
    @IBOutlet weak var rocketButton: UIButton!
    @IBOutlet weak var engineerButton: UIButton!
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

    }
    
    // MARK: - Alert
    func showAlert(_ title: String,_ subtitle: String){
        let alert = UIAlertController(title: title,
                                      message: subtitle,
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default))
        present(alert, animated: true)
    }

    // MARK: - Action
    @IBAction func astronoutButtonTapped(_ sender: Any) {
        showAlert("Your Job", "Astronout")
    }
    
    @IBAction func rockStarButtonTapped(_ sender: Any) {
        showAlert("Your Job", "Rock Star")
    }
    
    @IBAction func engineerButtonTapped(_ sender: Any) {
        showAlert("Your Job", "Engineer")
    }
    
    @IBAction func rocketButtonTapped(_ sender: Any) {
        showAlert("Your Job", "Rocket Scientist")
    }
}

